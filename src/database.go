/*
 *	database.go
 *	author: oyaro
 *	email: oleksandr.yarotskyi.work@gmail.com
 */

package main

import (
	"os"
	"fmt"
	"io/ioutil"
	"database/sql"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
)

// Return Database object with data from "database.json" file
func LoadDatabaseConfig() *Database {
	fp, err := os.Open("database.json")
	if err != nil {
		Error.Printf("[!] Помилка під час завантаження файла з конфігураціями:\n%s\n", err)
	}
	defer fp.Close()

	r, err := ioutil.ReadAll(fp) // read all from database.json file
	if err != nil {
		Error.Printf("[!] Помилка читання файла з конфігураціями:\n%s\n", err)
	}

	db := new(Database) // create Database object
	json.Unmarshal(r, db) // fill Database object from file

	return db
}

func RequestToDatabase(db *Database, query string) [][]string {
	conn, err := sql.Open(
		"mysql",
		fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", db.Login, db.Password, db.Host, db.Port, db.Name),
	)
	defer conn.Close()
	if err != nil {
		Error.Printf("[!] Помилка підключення до бази даних:\n%s\n", err)
	}

	rows, err := conn.Query(query)
	defer rows.Close()
	if err != nil {
		Warning.Printf("[!] Помилка запиту:\n%s\n", err)
	}

	var result [][]string
	cols, _ := rows.Columns()
	pointers := make([]interface{}, len(cols))
	container := make([]string, len(cols))

	for i, _ := range pointers {
		pointers[i] = &container[i]
	}

	for rows.Next() {
		rows.Scan(pointers...)
		result = append(result, container)
	}

	return result
}
