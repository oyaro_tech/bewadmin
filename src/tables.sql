CREATE TABLE Users (
        id              INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name            VARCHAR(120) NOT NULL,
        phone           VARCHAR(20) NOT NULL,
        email           VARCHAR(254),
        address         VARCHAR(254),
        paid            TIMESTAMP,
        reg_date        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE Intercoms (
        id              INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        login           VARCHAR(120) NOT NULL,
        password        VARCHAR(32) NOT NULL,
        ipaddr          VARCHAR(16) NOT NULL,
        port            INT(5) NOT NULL,
        address         VARCHAR(254) NOT NULL,
        reg_date        TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
