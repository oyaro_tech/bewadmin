/*
 *	main.go
 *	author: oyaro
 *	email: oleksandr.yarotskyi.work@gmail.com
 */

package main

import (
	"os"
	"log"
	"net/http"
)

var (
	Info 	= log.New(os.Stdout, "[ІНФОРМАЦІЯ]: ", 		log.Ldate|log.Ltime|log.Lshortfile)
	Warning = log.New(os.Stderr, "[ПОПЕРЕЖДЕННЯ]: ", 	log.Ldate|log.Ltime|log.Lshortfile)
	Error 	= log.New(os.Stderr, "[ПОМИЛКА]: ", 		log.Ldate|log.Ltime|log.Lshortfile)
	db = LoadDatabaseConfig()
)

	const DEBUG = true

func main() {
	// Ctrl+C handler
	SetupCloseHandler()

	http.HandleFunc("/", 				Index)
	http.HandleFunc("/intercoms", 		Intercoms)
	http.HandleFunc("/createIntercom", 	CreateIntercom)
	http.HandleFunc("/users", 			Users)
	http.HandleFunc("/favicon.ico", 	Favicon)

	Info.Println("Сервер запущено.")
	log.Fatal(http.ListenAndServe(":80", nil))
}
