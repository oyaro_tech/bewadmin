/*
 *	server.go
 *	author: oyaro
 *	email: oleksandr.yarotskyi.work@gmail.com
 */

package main

import (
	"fmt"
	"html"
	"time"
	"net/http"
	"database/sql"
	"html/template"
	_ "github.com/go-sql-driver/mysql"
)


func Index(w http.ResponseWriter, r *http.Request) {
	Info.Printf("[Клієнт %q] BewAdmin: %s\n", r.RemoteAddr, html.EscapeString(r.URL.Path))

	data := SiteData{
		Title: "BewAdmin Головна",
		Year: time.Now().Year(),
	}

	tmpl, _ := template.ParseFiles("templates/index.html")
	if DEBUG {Info.Printf("[Клієнт %q] відправка шаблона: \"templates/index.html\"\n", r.RemoteAddr, )}

	tmpl.Execute(w, data)
}

func Intercoms(w http.ResponseWriter, r *http.Request) {
	Info.Printf("[Клієнт %q] BewAdmin: %s\n", r.RemoteAddr, html.EscapeString(r.URL.Path))

	tmpl, _ := template.ParseFiles("templates/intercoms.html")
	if DEBUG {Info.Printf("[Клієнт %q] відправка шаблона: \"templates/intercoms.html\"\n", r.RemoteAddr, )}


	conn, err := sql.Open(
		"mysql",
		fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", db.Login, db.Password, db.Host, db.Port, db.Name),
	)
	defer conn.Close()
	if err != nil {
		Error.Printf("[!] Помилка підключення до бази даних:\n%s\n", err)
	}

	rows, err := conn.Query("SELECT id, login, password, ipaddr, port, address, DATE_FORMAT(reg_date, '%d.%m.%Y') FROM Intercoms;")
	defer rows.Close()
	if err != nil {
		Warning.Printf("[!] Помилка запиту:\n%s\n", err)
	}

	intercoms := []Intercom{}

	for rows.Next() {
		intercom := new(Intercom)

		err := rows.Scan(
			&intercom.ID,
			&intercom.Login,
			&intercom.Password,
			&intercom.Ipaddr,
			&intercom.Port,
			&intercom.Address,
			&intercom.Reg_date,
		)
		if err != nil {
			Error.Printf("[!] Помилка збереження результату запиту:\n%s\n", err)
			Error.Fatal()
        }

		intercoms = append(intercoms, *intercom)
	}

	data := SiteData{
		Title: "BewAdmin IP-Відеопанелі",
		Year: time.Now().Year(),
		Intercoms: intercoms,
	}

	tmpl.Execute(w, data)
}

func Users(w http.ResponseWriter, r *http.Request) {
	Info.Printf("[Клієнт %q] BewAdmin: %s\n", r.RemoteAddr, html.EscapeString(r.URL.Path))

	tmpl, _ := template.ParseFiles("templates/users.html")
	if DEBUG {Info.Printf("[Клієнт %q] відправка шаблона: \"templates/users.html\"\n", r.RemoteAddr, )}


	conn, err := sql.Open(
		"mysql",
		fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", db.Login, db.Password, db.Host, db.Port, db.Name),
	)
	defer conn.Close()
	if err != nil {
		Error.Printf("[!] Помилка підключення до бази даних:\n%s\n", err)
	}

	rows, err := conn.Query("SELECT id, name, phone, email, address, DATE_FORMAT(paid, '%d.%m.%Y'), DATE_FORMAT(reg_date, '%d.%m.%Y') FROM Users;")
	defer rows.Close()
	if err != nil {
		Warning.Printf("[!] Помилка запиту:\n%s\n", err)
	}

	users := []User{}

	for rows.Next() {
		user := new(User)

		err := rows.Scan(
			&user.ID,
			&user.Name,
			&user.Phone,
			&user.Email,
			&user.Address,
			&user.Paid,
			&user.Reg_date,
		)
		if err != nil {
			Error.Printf("[!] Помилка збереження результату запиту:\n%s\n", err)
			Error.Fatal()
        }

		users = append(users, *user)
	}

	data := SiteData{
		Title: "BewAdmin Абоненти",
		Year: time.Now().Year(),
		Users: users,
	}

	tmpl.Execute(w, data)
}

func Favicon(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "favicon.ico")
}
