/*
 *	data.go
 *	author: oyaro
 *	email: oleksandr.yarotskyi.work@gmail.com
 */

package main

type Database struct {
	Host 		string 	`json:"host"`
	Port 		int 	`json:"port"`
	Name 		string 	`json:"name"`
	Login 		string 	`json:"login"`
	Password 	string 	`json:"password"`
}

type User struct {
	ID			int64
	Name		string
	Phone		string
	Email		string
	Address		string
	Paid		string
	Reg_date	string
}

type Intercom struct {
	ID			int64
	Login		string
	Password	string
	Ipaddr		string
	Port		int64
	Address		string
	Reg_date	string
}

type SiteData struct {
	Title 		string
	Year		int
	Users		[]User
	Intercoms 	[]Intercom
}

