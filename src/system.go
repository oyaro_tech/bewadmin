/*
 *	system.go
 *	author: oyaro
 *	email: oleksandr.yarotskyi.work@gmail.com
 */

package main

import (
	"os"
	"os/signal"
	"syscall"
)

// Ctrl+C handler
func SetupCloseHandler() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		Info.Println("\rСервер зупинено.")
		os.Exit(0)
	}()
}
